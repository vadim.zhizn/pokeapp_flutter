class ShortInfo{
  final String name;
  final String url;

  ShortInfo(
    {
      required this.name,
      required this.url,
    }
  );
}