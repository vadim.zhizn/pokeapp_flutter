import 'package:flutter/material.dart';

class Characteristics{
  final List<String> abilities;
  final int height;
  final int weight;
  final int baseExperience;
  final String name;

  Characteristics(
    {
      required this.abilities,
      required this.height,
      required this.weight,
      required this.baseExperience,
      required this.name
    }
  );
  @override
  String toString() {
    String output = "Name: $name\nHeight: $height\nWeight: $weight\nBase experience: ${baseExperience}\nAbilities:\n";
    for(var ability in abilities){
      output += ability + "\n";
    }
    return output;
  }
}
