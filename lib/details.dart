import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'models/pokemon_characts.dart';


class DetailsPage extends StatefulWidget{
  int index = 0;
  DetailsPage(int index){
    this.index = index;
  }
  @override
  _DetailsPageState createState() => _DetailsPageState(index);
}

class _DetailsPageState extends State<DetailsPage>{
  Characteristics characts = Characteristics(abilities: [], height: 0, weight: 0, baseExperience: 0, name: "");
  int index = 0;
  _DetailsPageState(int index){
    this.index = index;
  }

  @override
  void initState() {
    fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: Text('The details page')),
      body: Column(
        children:<Widget>[
        Image(image: NetworkImage('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/$index.png', scale: 0.33),
          ),
          Container(
            padding: EdgeInsets.all(15.0),
            child: Text(characts.toString(), style: TextStyle(fontSize: 30))
          )
        ]
      ),
    );
  }

  fetch() async{
    final response = await http.get(Uri.parse('https://pokeapi.co/api/v2/pokemon/$index'));
    Map mapResponse = json.decode(response.body);
    List<String> abilities = [];

    if(response.statusCode == 200){
      String name = mapResponse['name'];
      int weight = mapResponse['weight'];
      int height = mapResponse['height'];
      int baseExperience = mapResponse['base_experience'];
      for(var u in mapResponse['abilities']){
        abilities.add(u['ability']['name']);
      }
      setState(() {
        characts = new Characteristics(abilities: abilities, height: height, weight: weight, baseExperience: baseExperience, name: name);
      });
    }else{
      throw Exception();
    }
  }
}