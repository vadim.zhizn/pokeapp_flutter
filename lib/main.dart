import 'dart:convert';
import 'details.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models/short_pokemon_info.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext buildContext){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Pokemons",
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage()
    );
  }
}

class MyHomePage extends StatefulWidget{
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>{
  List<ShortInfo> shortPokes = [];
  ScrollController _scrollController = new ScrollController();
  int offset = 0;
  int count = 0;

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    fetch();
    _scrollController.addListener(() {
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){
        if(offset < count && count != 0)
        {
          offset+=20;
        }
        fetch();
      }
    });
  }

  @override
  Widget build(BuildContext buildContext){
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Pokemons"),
        ),
        body: ListView.separated(
          controller: _scrollController,
          itemCount: shortPokes.length,
          itemBuilder: (BuildContext context, int index){
            return ListTile(
              title: Text((index+1).toString() + ". " + shortPokes[index].name),
              trailing: Icon(Icons.arrow_forward),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DetailsPage(index + 1)));
              },);
          },
          separatorBuilder: (_, __) => Divider(color: Colors.black),
        )
      );
  }

  fetch() async{
    final response = await http.get(Uri.parse('https://pokeapi.co/api/v2/pokemon?limit=20&offset=${offset}'));
    Map mapResponse = json.decode(response.body);
    List<ShortInfo> newData = [];
    count = mapResponse['count'];
    for(var u in mapResponse['results']){
      newData.add(ShortInfo(name: u['name'], url: u['url']));
    }
    if(response.statusCode == 200){
      setState((){
        shortPokes.addAll(newData);
      });
    }else{
      throw Exception();
    }
  }
}